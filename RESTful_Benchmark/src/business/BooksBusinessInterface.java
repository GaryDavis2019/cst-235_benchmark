package business;
import java.util.List;

import javax.ejb.*;
import beans.*;

@Local
public interface BooksBusinessInterface {
	//Needed Methods for Books Business Services
	public List<Book> getBooks();
	public void setBooks(List<Book> books);

}
