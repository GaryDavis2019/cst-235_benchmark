package business;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import javax.ws.rs.*;

import beans.Book;

@RequestScoped
@Path("/books")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })


public class BibleRestService {
	
	//Injecting the Book Business Interface
	@Inject
	private business.BooksBusinessInterface service;
	
	public BibleRestService(){
		
	}
	
	//The get JSON Method
	@GET
	@Path("/getjson")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Book> getOrdersAsJson(){
		return service.getBooks();
		
	}
	
	//The get XML Method
	@GET
	@Path("/getxml")
	@Produces(MediaType.APPLICATION_XML)
	public Book[] getOrdersAsXml() {
		Book[] tmpBook = new Book[service.getBooks().size()];
		for (int x=0; x<service.getBooks().size(); x++) {
			tmpBook[x] = service.getBooks().get(x);
		}
		return tmpBook;
	}
}
