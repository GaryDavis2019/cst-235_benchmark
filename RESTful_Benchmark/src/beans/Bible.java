package beans;
import javax.faces.bean.*;


@ManagedBean 
@SessionScoped
public class Bible {
	String bookNumber;
	String chapterNumber;
	String verseNumber;
	String verseText;

	/** Constructors */ 
	public Bible() { 
		this.bookNumber = "1";
		this.chapterNumber = "1";
		this.verseNumber = "1";
		this.verseText = "";
	}
	
	public Bible(String bookNumber, String chapterNumber, String verseNumber, String verseText) {
		this.bookNumber = bookNumber;
		this.chapterNumber = chapterNumber;
		this.verseNumber = verseNumber;
		this.verseText = verseText;
	}

	
	//getters and setters	
	public String getBookNumber() {
		return bookNumber;
	}

	public void setBookNumber(String bookNumber) {
		this.bookNumber = bookNumber;
	}

	public String getChapterNumber() {
		return chapterNumber;
	}

	public void setChapterNumber(String chapterNumber) {
		this.chapterNumber = chapterNumber;
	}

	public String getVerseNumber() {
		return verseNumber;
	}

	public void setVerseNumber(String verseNumber) {
		this.verseNumber = verseNumber;
	}

	public String getVerseText() {
		return verseText;
	}

	public void setVerseText(String verseText) {
		this.verseText = verseText;
	}

}
